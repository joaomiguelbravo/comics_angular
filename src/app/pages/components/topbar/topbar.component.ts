import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ComicsService} from '../../../services/comics.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {

  @Output() searchEvent = new EventEmitter<string>();

  searchResult: string;

  constructor(
    public comicService: ComicsService
  ) { }

  ngOnInit() {}

  triggerSearchRequest() {
    if (this.searchResult !== undefined) {
      this.searchEvent.emit(this.searchResult);
    }
  }

}
