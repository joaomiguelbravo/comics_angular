import { Component, OnInit } from '@angular/core';
import {ComicsService} from '../../services/comics.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-comics-list',
  templateUrl: './comics-list.component.html',
  styleUrls: ['./comics-list.component.scss']
})
export class ComicsListComponent implements OnInit {

  comicsList: any;
  isLoadingResults = true;
  totalRecords = 0;
  pageSize = 40;
  offSet = 0;
  comicTitle: string;

  constructor(
    public comicService: ComicsService
  ) { }

  async ngOnInit() {
    await this.getComicsList();
  }

  async getComicsList() {
    let response;
    const data = { title: this.comicTitle, limit: this.pageSize, offset: this.offSet };
    this.isLoadingResults = true;
    response = await this.comicService.getComicsList(data);
    this.comicsList = response.data.results;
    this.isLoadingResults = false;
    this.handlePaginator(response.data);
  }

  async getComicsListByTitle(event) {
    this.comicTitle = event;
    await this.getComicsList();
  }

  handlePaginator(data) {
    this.totalRecords = data.total;
    this.pageSize = data.limit;
  }

   paginatorOutput(event) {
    this.pageSize = event.pageSize;
    this.offSet += this.pageSize + 1;
    this.getComicsList();
  }

  handleClickedCard(comicData) {
    this.comicService.updateComic(comicData);
    this.getComicsList();
  }
}
