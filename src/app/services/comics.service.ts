import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {HttpParams} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class ComicsService {


  constructor(
    private apiService: ApiService,
    private matSnackBar: MatSnackBar
  ) { }

  async getComicsList(data) {
    const url = this.getUrl('comics');
    const params = this.buildParams(data);
    const response = await this.apiService.get(url, { params });

    this.responseNotification(response);

    return response.body;
  }

  updateComic(params ) {
    const url = this.getUrl(`comics/${params.comic_id}`);

    this.apiService.put(url, params);
  }


  buildParams(data) {
    let httParams = new HttpParams();
    Object.keys(data).forEach((key) => {
      if (!!data[key]) {
        httParams = httParams.set(key, data[key]);
      }
    });

    return httParams;
  }

  getUrl(extension= '') {
    return `api/v1/${extension}`;
  }

  responseNotification(response){
    if (response.status === 200) {
      this.openSnackBar('Everything good with the request!');
    } else {
      this.openSnackBar('Something went wrong');
    }
  }

  openSnackBar(message) {
    this.matSnackBar.open(message, '', {
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      duration: 1000
    });
  }
}
