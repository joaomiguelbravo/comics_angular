import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ComicsService} from '../../../services/comics.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() comic: any;
  @Output() favoriteClickEvent = new EventEmitter<any>();

  thumbnail: any;
  imageUrl: string;
  imageSrc = 'assets/img/heart_off.png';

  constructor() { }

  ngOnInit() {
    this.thumbnail = this.comic.thumbnail;
    this.imageUrl = `${this.thumbnail.path}/portrait_xlarge.${this.thumbnail.extension}`;
  }

  handleTitle(title) {
    return title.slice(0, 27);
  }

  setComicAsFavorite(value) {
    const data = { comic_id: this.comic.id, favorite: value };
    this.favoriteClickEvent.emit(data);
  }

}
